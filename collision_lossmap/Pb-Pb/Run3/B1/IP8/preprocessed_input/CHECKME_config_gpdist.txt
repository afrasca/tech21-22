*e**************************************************************************
** Produced on 17-10-2022 by make_beam_marker.py
** Automatically generated input - CHECK ALL PARAMETERS before running!
***************************************************************************
* INPUT GENERATION FOR PYFLUSIX [2019]
***************************************************************************

**** OPTICS PARAMETERS ****************************************************
***************************************************************************
****    Optics/Beam parameters    ****************************************
*
*  WHAT(1) : index of the plane
*           1 -> horizonal;  2 -> vertical;
*    WHAT(2) : Beta function [m]  
*    WHAT(3) : derivative of Beta function []
*    WHAT(4) : Dispersion function [m]
*    WHAT(5) : derivative of Dispersion function []
*    WHAT(6) : Normalized emittance [m]
*           3 -> longitudinal;
*    WHAT(2) : particle mass [GeV/c2]  
*    WHAT(3) : momentum of synchronous particle [GeV/c]
*    WHAT(4) : sigma delta_p/p []
*    WHAT(5) : bunch length (actually 4*sigma_l) [s]
*    WHAT(6) : not used
*           4 -> ion specification (only for ion beams)
*    WHAT(2) : ion atomic mass (A)
*    WHAT(3) : ion atomic number (Z)
*
***************************************************************************
** Beam at marker IP8
*
PARAM     1        1.477635  -0.016771   -0.003754  0.001194   1.6e-06
PARAM     2        1.48685  -0.001257   -0.027239  0.012925   1.6e-06
PARAM     3        193.687690162648  574000.0   0.000106   1.1e-09
PARAM     4        208 82 82
*PARAM     5        0  82
*
***************************************************************************

**** SAMPLING DISTRIBUTIONS ***********************************************
*************************************************************************** 
*
DTYPE     1         GAUSS         
DTYPE     2         GAUSS         
DTYPE     3         PENCIL         
*
***************************************************************************

**** OFFSETS **************************************************************
***************************************************************************
*
OFFSET      1.1e-05  0.000299  -0.0  -2e-06  0.0  0.0
*
***************************************************************************

****    Geometrical Acceptance ********************************************
*
*  WHAT(1) : 1: x; 2: xp;
*          : 3: y; 4: yp;
*          : 5: t; 6: pc;
*  WHAT(2) : cut expressed in normalised (N) or real (R) units
*            - N:  sig;
*            - R:  x/y:   m
*                  xp/yp: rad
*                  t:     s
*                  pc:    GeV
*  WHAT(3) : min value
*  WHAT(4) : max value
*  WHAT(5) : (optional) check on absolute value (A)
*  WHAT(6) : (optional) check on absolute value performed after adding
*            offsets (A)
*
***************************************************************************
*
*GEOACC      EDITME     R     EDITME     EDITME   A
*
***************************************************************************

**** S SHIFT **************************************************************
*************************************************************************** 
*  WHAT(1) : length of drift;
*          : > 0 the final point is downstream of where the sampling is done 
*          : < 0 the final point is upstream of where the sampling is done
*  WHAT(2) : (optional, no default) name of file where to dump the orginal
*            distribution before being shifted
*  WHAT(3) : (opional, default 0) type of tracking along the drift
*            0: approximated Hamiltonian, corresponding to FAST compilation
*                flag of SixTrack
*            1: exact Hamiltonian
*            2: approximated Hamiltonian, corresponding the non -=FAST=
*                compilation flag of SixTrack
***************************************************************************
*
*SSHIFT     EDITME    unshifted_initial.dat
*
***************************************************************************

**** SAMPLE PARAMETERS ****************************************************
***************************************************************************
*
OUTPUT  100  RNGSEED  initial.dat header.dat 0 1
*
***************************************************************************
