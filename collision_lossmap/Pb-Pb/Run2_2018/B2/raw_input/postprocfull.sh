#!/bin/bash

gzip *
gunzip tracker_* linopt_coupled*
cp -rf *.gz $RUNDIR/${NAMENUMDIR}
cp -rf fluka_* $RUNDIR/${NAMENUMDIR}
