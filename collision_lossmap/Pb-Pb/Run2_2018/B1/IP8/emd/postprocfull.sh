#!/bin/bash

python /afs/cern.ch/work/a/afrasca/private/py_scripts/clean_aperture_losses.py
mv fluka_isotope.log isotope.log
gzip *
cp fort.208.gz aperture_losses.dat.gz $RUNDIR/${NAMENUMDIR}
cp -rf fluka_* $RUNDIR/${NAMENUMDIR}
