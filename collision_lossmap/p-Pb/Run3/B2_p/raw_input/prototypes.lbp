#
# ------------------------------------------------------------------------------
# primary collimators
# ------------------------------------------------------------------------------
#
# model with jaw in CFC
ASSEMBLY      TCP
FEDB_SERIES   lhc
FEDB_TAG      TCP
ROT-DEFI         0.0       0.0       0.0    200.0   -3000.0    1000.0 proto
* skew angle
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle CONTAINO
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_NEG
* Jaw opening
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_NEG
#
# ------------------------------------------------------------------------------
# secondary collimators
# ------------------------------------------------------------------------------
#
# model with jaw in CFC
ASSEMBLY      TCSG
FEDB_SERIES   lhc
FEDB_TAG      TCSG
ROT-DEFI         0.0       0.0       0.0    -200.0   -3000.0   2000.0 proto
* skew angle
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle CONTAINO
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_NEG
* Jaw opening
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_NEG
#
# model of TCSPs in IR6
ASSEMBLY      TCSP
FEDB_SERIES   lhc
FEDB_TAG      TCSP
ROT-DEFI         0.0       0.0       0.0     400.0   -3000.0   2000.0 proto
* skew angle
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle CONTAINO
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_NEG
* Jaw opening
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_NEG
#
# ------------------------------------------------------------------------------
# shower absorbers
# ------------------------------------------------------------------------------
#
ASSEMBLY      TCLA
FEDB_SERIES   lhc
FEDB_TAG      TCLA
ROT-DEFI         0.0	   0.0       0.0       0.0   -3000.0   3000.0 proto
* skew angle
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle CONTAINO
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_NEG
* Jaw opening
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_NEG
#
ASSEMBLY      TCLD
FEDB_SERIES   hilumi
FEDB_TAG      TCLD
ROT-DEFI         0.0	   0.0       0.0     200.0   -3000.0   3000.0 proto
* skew angle
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle CONTAINO
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_NEG
* Jaw opening
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_NEG
MAP_ENTRIES   TCLD.9R7.B1
# ------------------------------------------------------------------------------
# tertiary collimators and physics debris absorbers
# ------------------------------------------------------------------------------
#
# TCTPs
ASSEMBLY      TCTPV
FEDB_SERIES   lhc
FEDB_TAG      TCT
ROT-DEFI         0.0       0.0       0.0       0.0   -3000.0   4000.0 proto
* skew angle
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle CONTAINO
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_NEG
* Jaw opening
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_NEG
#
ASSEMBLY      TCTPH
FEDB_SERIES   lhc
FEDB_TAG      TCT
* skew angle
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle CONTAINO
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_NEG
* Jaw opening
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_NEG
MAP_ENTRIES   TCL.4L5.B2 TCL.6L1.B2 TCL.6L5.B2 TCL.6R1.B1 TCL.6R5.B1
#
# TCL (Cu jaw)
ASSEMBLY      TCL
FEDB_SERIES   lhc
FEDB_TAG      TCL
ROT-DEFI         0.0       0.0       0.0     200.0   -3000.0   4000.0 proto
* skew angle
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle CONTAINO
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_NEG
* Jaw opening
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_NEG
MAP_VETOS     TCL.4L5.B2
#
# ------------------------------------------------------------------------------
# injection protection devices
# ------------------------------------------------------------------------------
#
ASSEMBLY      TCLIA
FEDB_SERIES   lhc
FEDB_TAG      TCLIA
ROT-DEFI         0.0       0.0       0.0    -200.0   -3000.0   5000.0 proto
* skew angle
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle CONTAINO
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_NEG
* Jaw opening
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_NEG
#
ASSEMBLY      TCLIB
FEDB_SERIES   lhc
FEDB_TAG      TCLIB
ROT-DEFI         0.0       0.0       0.0       0.0   -3000.0   5000.0 proto
* skew angle
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle CONTAINO
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_NEG
* Jaw opening
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_NEG
#
ASSEMBLY      TDI
FEDB_SERIES   lhc
FEDB_TAG      TDI
ROT-DEFI         0.0       0.0       0.0     200.0   -3000.0   5000.0 proto
* skew angle
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle CONTAINO
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 3 -1. rotat angle JAW_NEG
* Jaw opening
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_POS
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap JAW_NEG
#
# ------------------------------------------------------------------------------
# extraction protection devices
# ------------------------------------------------------------------------------
#
ASSEMBLY      TCDQAA
FEDB_SERIES   lhc
FEDB_TAG      TCDQnAA
ROT-DEFI         0.0       0.0       0.0   -200.0   -3000.0    6000.0 proto
* Jaw opening
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDAB01
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDAB02
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDAB03
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDAB04
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDAB05
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDAB06
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDAB07
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDAB08
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDAB09
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDAB10
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDAB11
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDAB12
MAP_ENTRIES   TCDQA.A4R6.B1 TCDQA.A4L6.B2
#
ASSEMBLY      TCDQAB
FEDB_SERIES   lhc
FEDB_TAG      TCDQnAB
ROT-DEFI         0.0       0.0       0.0      0.0   -3000.0    6000.0 proto
* Jaw opening
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDBB01
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDBB02
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDBB03
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDBB04
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDBB05
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDBB06
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDBB07
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDBB08
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDBB09
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDBB10
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDBB11
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDBB12
# NB: in twiss sequence, TCDQA.C is before TCDQA.B
MAP_ENTRIES   TCDQA.C4R6.B1 TCDQA.C4L6.B2
#
ASSEMBLY      TCDQAC
FEDB_SERIES   lhc
FEDB_TAG      TCDQnAC
ROT-DEFI         0.0       0.0       0.0    200.0   -3000.0    6000.0 proto
* Jaw opening
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDCB01
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDCB02
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDCB03
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDCB04
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDCB05
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDCB06
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDCB07
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDCB08
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDCB09
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDCB10
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDCB11
ROT-DEFI_FROM_TWISS_SEQUENCE 1 -1. trasl halfgap RDCB12
# NB: in twiss sequence, TCDQA.C is before TCDQA.B
MAP_ENTRIES   TCDQA.B4R6.B1 TCDQA.B4L6.B2
#
# ------------------------------------------------------------------------------
# beam-beam / beam-target collisions
# ------------------------------------------------------------------------------
#
PROTOTYPE     IPPIPE
FEDB_SERIES   lhc
FEDB_TAG      IPPIPE
CONTAINER     IPPIPEO
ROT-DEFI         0.0       0.0       0.0      0.0   -3000.0    7000.0 proto
MAP_ENTRIES   IP1
#
