*e**************************************************************************
** Produced on 31-03-2022 by make_beam_marker.py
** Automatically generated input - CHECK ALL PARAMETERS before running!
***************************************************************************
* INPUT GENERATION FOR PYFLUSIX [2019]
***************************************************************************

**** OPTICS PARAMETERS ****************************************************
***************************************************************************
****    Optics/Beam parameters    ****************************************
*
*  WHAT(1) : index of the plane
*           1 -> horizonal;  2 -> vertical;
*    WHAT(2) : Beta function [m]  
*    WHAT(3) : derivative of Beta function []
*    WHAT(4) : Dispersion function [m]
*    WHAT(5) : derivative of Dispersion function []
*    WHAT(6) : Normalized emittance [m]
*           3 -> longitudinal;
*    WHAT(2) : particle mass [GeV/c2]  
*    WHAT(3) : momentum of synchronous particle [GeV/c]
*    WHAT(4) : sigma delta_p/p []
*    WHAT(5) : bunch length (actually 4*sigma_l) [s]
*    WHAT(6) : not used
*           4 -> ion specification (only for ion beams)
*    WHAT(2) : ion atomic mass (A)
*    WHAT(3) : ion atomic number (Z)
*
***************************************************************************
** Beam at marker IP2
*
PARAM     1        10.008108  0.0253238   -0.0045601  -0.0051570   1.9e-06
PARAM     2        10.009999  0.0261110   -0.0754072   0.0078039   1.9e-06
PARAM     3        0.93827231  6500.0   0.00011   1.1e-09
*PARAM     4        1 1 1
*PARAM     5        0  1
*
***************************************************************************

**** SAMPLING DISTRIBUTIONS ***********************************************
*************************************************************************** 
*
DTYPE     1         GAUSS         
DTYPE     2         GAUSS         
DTYPE     3         GAUSS         
*
***************************************************************************

**** OFFSETS **************************************************************
***************************************************************************
*
OFFSET      0.9999505e-3  -0.0000096e-3  -2.0502544e-3  0.2000131e-3  0.0  0.0
*
***************************************************************************

****    Geometrical Acceptance ********************************************
*
*  WHAT(1) : 1: x; 2: xp;
*          : 3: y; 4: yp;
*          : 5: t; 6: pc;
*  WHAT(2) : cut expressed in normalised (N) or real (R) units
*            - N:  sig;
*            - R:  x/y:   m
*                  xp/yp: rad
*                  t:     s
*                  pc:    GeV
*  WHAT(3) : min value
*  WHAT(4) : max value
*  WHAT(5) : (optional) check on absolute value (A)
*  WHAT(6) : (optional) check on absolute value performed after adding
*            offsets (A)
*
***************************************************************************
*
*GEOACC      EDITME     R     EDITME     EDITME   A
*
***************************************************************************

**** S SHIFT **************************************************************
*************************************************************************** 
*  WHAT(1) : length of drift;
*          : > 0 the final point is downstream of where the sampling is done 
*          : < 0 the final point is upstream of where the sampling is done
*  WHAT(2) : (optional, no default) name of file where to dump the orginal
*            distribution before being shifted
*  WHAT(3) : (opional, default 0) type of tracking along the drift
*            0: approximated Hamiltonian, corresponding to FAST compilation
*                flag of SixTrack
*            1: exact Hamiltonian
*            2: approximated Hamiltonian, corresponding the non -=FAST=
*                compilation flag of SixTrack
***************************************************************************
*
*SSHIFT     EDITME    unshifted_initial.dat
*
***************************************************************************

**** SAMPLE PARAMETERS ****************************************************
***************************************************************************
*
OUTPUT  10000  RNGSEED  initial.dat header.dat 0 1
*
***************************************************************************
