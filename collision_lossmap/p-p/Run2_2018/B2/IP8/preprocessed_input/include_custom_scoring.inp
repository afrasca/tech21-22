* ..+....1....+....2....+....3....+....4....+....5....+....6....+....7..
*
* Place in this file all the custom scoring you require
*
* ..+....1....+....2....+....3....+....4....+....5....+....6....+....7....+....8
USRBIN           8.0    ENERGY     -41.0  @LASTREG     500.0   10000.0EDREG
USRBIN           1.0       0.0  -10000.0       1.0       1.0       1.0&
*
* A.Lechner and A.Mereghetti, 2010/10/22
* new way to give back particles to Icosim via fluscw.f routine (no more usrmed)
*     through a fake USRBDX estimator
USERWEIG                             3.0
* ..+....1....+....2....+....3....+....4....+....5....+....6....+....7....+....8
USRBDX          99.0    PROTON     -42.0   VAROUND  TRANSF_D          BACK2ICO
*
* All charged particles
* NB: if you use this one, comment out USRBDX cards targeting specific particles!
* ..+....1....+....2....+....3....+....4....+....5....+....6....+....7....+....8
* USRBDX          99.0  ALL-CHAR     -42.0   VAROUND  TRANSF_D          BACK2ICO
*
* A.Mereghetti, 2018/07/17
* in case of heavy ions, please un-comment the following lines:
* ..+....1....+....2....+....3....+....4....+....5....+....6....+....7....+....8
* USRBDX          99.0  HEAVYION     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0  4-HELIUM     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0  3-HELIUM     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0    TRITON     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0  DEUTERON     -42.0   VAROUND  TRANSF_D          BACK2ICO
*
* Additional stable particles
* ..+....1....+....2....+....3....+....4....+....5....+....6....+....7....+....8
* USRBDX          99.0   APROTON     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0  ELECTRON     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0  POSITRON     -42.0   VAROUND  TRANSF_D          BACK2ICO
*
* Unstable particles
* ..+....1....+....2....+....3....+....4....+....5....+....6....+....7....+....8
* USRBDX          99.0     MUON+     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0     MUON-     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0     PION+     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0     PION-     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0     KAON+     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0     KAON-     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0    SIGMA-     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0    SIGMA+     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0   ASIGMA-     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0   ASIGMA+     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0      XSI-     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0     AXSI+     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0    OMEGA-     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0   AOMEGA+     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0      TAU+     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0      TAU-     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0        D+     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0        D-     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0       DS+     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0       DS-     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0  LAMBDAC+     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0     XSIC+     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0    XSIPC+     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0  ALAMBDC-     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0    AXSIC-     -42.0   VAROUND  TRANSF_D          BACK2ICO
* USRBDX          99.0   AXSIPC-     -42.0   VAROUND  TRANSF_D          BACK2ICO
*
* N.Fuster Martinez and A.Mereghetti, 2019-10-24
* the following cards are important for taking into account particles leaving
*   the collimator which are not sent back to SixTrack
* relevant especially for ions
* ..+....1....+....2....+....3....+....4....+....5....+....6....+....7..
* USRBDX          99.0    PROTON     -42.0   VAROUND  TRANSF_D          BCKFORT66
* USRBDX          99.0    PION+      -42.0   VAROUND  TRANSF_D          BCKFORT66
* USRBDX          99.0    PION-      -42.0   VAROUND  TRANSF_D          BCKFORT66
* USRBDX          99.0    KAON+      -42.0   VAROUND  TRANSF_D          BCKFORT66
* USRBDX          99.0    KAON+      -42.0   VAROUND  TRANSF_D          BCKFORT66
* USRBDX          99.0   NEUTRON     -42.0   VAROUND  TRANSF_D          BCKFORT66
* USRBDX          99.0   APROTON     -42.0   VAROUND  TRANSF_D          BCKFORT66
* USRBDX          99.0  POSITRON     -42.0   VAROUND  TRANSF_D          BCKFORT66
*
* A.Mereghetti, 2016/01/12
* touches!
* ..+....1....+....2....+....3....+....4....+....5....+....6....+....7....+....8
USERDUMP       100.0                                                           
*
